# Kodex Wflováku
*Jednoduchá příručka, jak být dobrým wflovakem*

Ahoj, jsme moc rádi, že ses rozhodl s námi do toho jít. Doufáme, že tě nezklameme a že pro tebe bude Wflow příjemné místo pro práci. Níže najdeš několik zásad pro to, aby se ti tu dobře makalo a aby ostatní Wflováci rádi makali s tebou.

## Základní předpoklady

Naše firemní kultura vychází z těchto základních předpokladů:

* Wflováci jsou dobří a féroví lidé
* Wflováci jsou motivováni přinášet hodnotu ostatním
* Wflováci chtějí nést zodpovědnost za věci, na kterých jim záleží (a mít související uznání, respekt a rozhodovací pravomoci)
* Bez štěstí není výkonů

## Upřímnost - Říkám věci tak, jak jsou

### Hej kámo kultura

Tykáme si a jsme neformální. Víme, že nejdůležitější je, jaký jsi lidsky a co umíš (naopak není důležité jak vypadáš, jestli jsi holka nebo kluk a čím sis předtím (ne)prošel). Pracovní život prožíváme spolu.

Když jsme tě nabírali, jeden z důvodů, proč jsi prošel pohovorem, byl ten, že si myslíme, že nám budeš sedět. Pokud ti nesedíme, žádný problém, prostě nám to řekni. Od toho je zkušebka, abychom to navzájem zjistili. :-)

### Když se mi něco nelíbí, řeknu to

Je důležité, abys řekl jinému Wflovákovi, že se ti něco nezdá nebo že se ti něco nelíbí. Ideálně to řekni rovnou tomu, kdo s tím může něco udělat. 

Pokud máš problém s jiným Wflovákem, tak to nedrž v sobě. Nejdřív se to pokus vyřešit v soukromí mezi čtyřma očima přímo s daným kolegou. Když se to nepodaří, vyhledejte společně Wflováka, kterému oba důvěřujete. Poproste ho, aby vám konflikt pomohl vyřešit.

Jedině upřímností a tím, že nebudeme věci zametat pod koberec, se nám podaří udržet prostředí, ve kterém se nám bude o věcech jednoduše mluvit a ve kterém bude radost pracovat.

### Dokážu pracovat se zpětnou vazbou

Pokud za tebou přijde někdo s konstruktivní zpětnou vazbou, oceň jeho snažení. Nebuď přehnaně defenzivní a snaž se pochopit, v čem se ti kolega či kolegyně snaží pomoct. Zamysli se nad tím, co bys mohl/a na základě této informace zlepšit.

## Důvěra

### Když něco slíbím, dodržím to. Moje sliby mají váhu

Když něco slíbím, Wflovákovi, zákazníkovi, partnerovi, snažím se to dodržet. Je důležité, aby ostatní lidi věděli, že se na tebe můžou spolehnout. Pokud si myslíš, že něco nemůžeš dodržet, neslibuj to (nauč se říkat ne), anebo řekni, za jakých podmínek to dodržíš.

Je ok slíbit, že se “na to dneska pokusíš podívat”, ale pokud to nevyjde, dej té druhé straně vědět a slib jim napodruhé takový termín, který už určitě zvládneš dodržet.
Selhání hlásím s předstihem (fail fast)
Když to vypadá, že se přes všechnu tvoji snahu něco nepovede dodržet, včas o tom tomu druhému dej vědět, aby se dle toho mohl zařídit.

Nebojím se říct si o pomoc. Když nevím tak se zeptám!

### Chválím veřejně

Pokud se někomu něco povede, řeknu to veřejně před ostatními; svá slova ale vážím, vím, že neupřímná pochvala je mnohem horší, než žádná pochvala. 

Tím, že sdílím úspěchy ostatních, přispívám k dobré atmosféře ve Wflow.

### Konflikt řeším přímo s tím, s kým ho mám, ne za jeho zády

Konflikty/pnutí mezi lidmi jsou přirozenou součástí fungování každé organizace a Wflow není výjimkou. To, jak lidé ke konfliktům přistupují, dělá rozdíl mezi organizací, kde je radost pracovat a místem, kde jsou vztahy na bodu mrazu.

Ve Wflow bereme pnutí jako příležitost osobnostně vyrůst a prohloubit vztahy se svými kolegy.

Následující postup se využívá pro všechny typy konfliktů: od nedodržování závazků a slibů, po porušení tohoto kodexu, přes profesní/technologickou neshodu až po osobní problém s kolegou.

Když mám s nějakým Wflovákem pnutí či konflikt, vždy ho nejdříve řeším mezi čtyřma očima přímo s ním.

Pokud nevím jak na to, postupuji dle [tohoto návodu](./managing-conflicts.md). 

Pokud se mi nepovede vyřešit konflikt ve dvou, dohodnu se s daným Wflovákem na někom, komu oba dva důvěřujeme. Poprosíme ho, aby nám dělal mediátora konfliktu.

Konflikt vezmeme před radu starších, pokud se nám nepovede konflikt vyřešit ve třech nebo se neshodneme na mediátorovi.

Konflikt je soukromá věc a měla by se stát veřejnou teprve tehdy, když tyto mechanismy selžou.

Wflovák by se neměl vyhýbat řešení konfliktů s ostatními. Pokud je mu to nepříjemné, může se jít poradit s radou starších.

### Nejsem toxický

Nepoužívám svoje emoce jako zbraň proti ostatním. Nelžu, nezkresluji realitu, nešířím pomluvy, nekonspiruji.

Pokud si všimnu, že se někdo z Wflováku chová toxicky, soukromě ho na to upozorním.

## Řemeslnost - Jsem dobrej ve svým oboru. Dělám to nejlíp, dle svého vědomí a svědomí

### Dělám věci “by the book”

Znám standardy ve svém oboru a dělám věci tak, jak se mají dělat. Nešidím to. Pokud mě někdo požádá o to, abych to ošidil, řeknu mu, že mě o takovou věc žádá. Pokud se jedná o věc podstatnou (bezpečnost, výkon kritické části aplikace, použitelnost klíčové funkcionality…), hájím poctivý způsob práce, byť to znamená jít do konfliktu s Wflovákem nebo zákazníkem.

Pokud tuším, že neznám best practices pro danou problematiku, proaktivně pracuji na tom, abych si je dostudoval. Vždy je dobré věci konzultovat se zkušenějším Wflovákem.

### Chci bejt dobrej a něco pro to dělám, nebojím se nových věcí

*"Learn or die"*

Neustále se vzdělávám ve svém řemeslu, sleduji trendy a osvojuji si nové postupy. Nebojím se jít za rámec své hlavní profese, abych porozuměl a dokázal lépe spolupracovat s technologiemi a lidmi kolem sebe (frontenďák se přiučí backendu a naopak, obchodník nebo support zaučí lidskou psychologii a t.d.).

Pracuji také na svých soft skillech, abych byl vyzrálejší osobností. Snažím se rozvíjet své lidství. Je v mém zájmu, aby lidé okolo mě se mnou pracovali rádi a abych se k nim choval tak, jak bych chtěl, aby se chovali oni ke mně.

### S ostatními chci sdílet to, co umím

*"Teach or die"*

Jsem hrdý na to, co umím. Rád sdílím to, co jsem se naučil s ostatními. Ať už je to ve formě workshopů, meetupů nebo jednoduchého one-on-one mentoringu. Vím, že je pro můj úspěch a úspěch Wflow důležité, když mám chytrého kolegu, který mě může podržet ve chvíli, kdy to potřebuju.