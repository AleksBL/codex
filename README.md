# Firemní kultura Wflow

## 🚩 Naše Vize a Mise (TODO)

## Kodexy Wflow

Níže najdete interní dokumenty Appliftingu, dle kterých se u nás řídíme.

1. [**Kodex Wflováka**](./cs/wflovers-codex.md) - Základní dokument, shrnující naše hodnoty
2. [**Příručka pro řešení konfliktů**](./cs/managing-conflicts.md) - V tomto dokumentu je popis procesu, který se ve Wflow používá pro řešení pracovních i osobních konfliktů
3. [**Příručka pro získání zpětné vazby**](./cs/getting-feedback.md) a [**Příručka pro dávání zpětné vazby**](./cs/giving-feedback.md) - Tyto dvě příručky popisují, jak si říct o zpětnou vazbu, jak s ní pracovat, ale i jak jí dávat
